#include <stdio.h>
#include <assert.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "program.h"
#include "utility.h"

GLuint graphics_program = 0;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);

void window_size_callback(GLFWwindow* window, int width, int height);

void main(void)
{
    GLFWwindow* window;
    // Initialise GLFW
    if (!glfwInit())
        return;

    int window_type = 1;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    if(window_type == 0)
    {
        GLFWmonitor* monitor = glfwGetPrimaryMonitor();
        const GLFWvidmode* mode = glfwGetVideoMode(monitor);
        glfwWindowHint(GLFW_RED_BITS, mode->redBits);
        glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
        glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
        glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate); 
        //glfwWindowHint(GLFW_SAMPLES, 4);

        // Create a fullscreen mode window and its OpenGL context
        //window = glfwCreateWindow(mode->width, mode->height, "OGL-TEST", monitor, NULL);
        window = glfwCreateWindow(1920, 1080, "OGL-TEST", monitor, NULL);
    }
    else if (window_type == 1)
    {
        int initial_width = 1280;
        int initial_height = 720;

        // Create a windowed mode window and its OpenGL context 
        window = glfwCreateWindow(initial_width, initial_height, "OGL-TEST", NULL, NULL);
    }

    if (!window)
    {
        glfwTerminate();
        return;
    }
    
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glfwSetWindowSizeCallback(window, &window_size_callback);
    glfwSetKeyCallback(window, &key_callback);

    // Make the window's context current
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();

    if (GLEW_OK != err)
        return;

    GLint major = 0;
    GLint minor = 0;
    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);
    const GLubyte* version = glGetString(GL_VERSION);
    const GLubyte* renderer = glGetString(GL_RENDERER);

    // Initialize 
    build_graphics_program(&graphics_program);

    // create 12 vertices of a icosahedron
    GLfloat t = (1.0 + sqrt(5.0)) / 2.0;
    GLfloat vertices[] = {
        -1,  t,  0,
    0.5,  0.5, 0.5,
         1,  t,  0,
    0.5,  0.5, 0.5,
        -1, -t,  0,
    0.5,  0.5, 0.5,
         1, -t,  0,
    0.5,  0.5, 0.5,

         0, -1,  t,
    0.5,  0.5, 0.5,
         0,  1,  t,
    0.5,  0.5, 0.5,
         0, -1, -t,
    0.5,  0.5, 0.5,
         0,  1, -t,
    0.5,  0.5, 0.5,

         t,  0, -1,
    0.5,  0.5, 0.5,
         t,  0,  1,
    0.5,  0.5, 0.5,
        -t,  0, -1,
    0.5,  0.5, 0.5,
        -t,  0,  1,
    0.5,  0.5, 0.5,
    };
    /*GLfloat vertices[] = {
        -1,  t,  0,
         1,  t,  0,
        -1, -t,  0,
         1, -t,  0,

         0, -1,  t,
         0,  1,  t,
         0, -1, -t,
         0,  1, -t,

         t,  0, -1,
         t,  0,  1,
        -t,  0, -1,
        -t,  0,  1,
    };
    GLfloat color[] = {
    0.5,  0.5, 0.5,
    0.5,  0.5, 0.5,
    0.5,  0.5, 0.5,
    0.5,  0.5, 0.5,

    0.5,  0.5, 0.5,
    0.5,  0.5, 0.5,
    0.5,  0.5, 0.5,
    0.5,  0.5, 0.5,

    0.5,  0.5, 0.5,
    0.5,  0.5, 0.5,
    0.5,  0.5, 0.5,
    0.5,  0.5, 0.5,
    };*/
    GLuint indices[] = {
        // 5 faces around point 0
        0, 11, 5,
        0, 5, 1,
        0, 1, 7,
        0, 7, 10,
        0, 10, 11,

        // 5 adjacent faces
        1, 5, 9,
        5, 11, 4,
        11, 10, 2,
        10, 7, 6,
        7, 1, 8,

        // 5 faces around point 3
        3, 9, 4,
        3, 4, 2,
        3, 2, 6,
        3, 6, 8,
        3, 8, 9,

        // 5 adjacent faces
        4, 9, 5,
        2, 4, 11,
        6, 2, 10,
        8, 6, 7,
        9, 8, 1,
    };

    err = glGetError(); assert(!err);

    GLuint vao;
    GLuint vbo;
    GLuint ibo;

    GLuint vertices_count = sizeof(vertices) / sizeof(vertices[0]) * 3;
    GLuint indices_count = sizeof(indices) / sizeof(indices[0]);

    glCreateBuffers(1, &vbo);
    glNamedBufferStorage(vbo, vertices_count * sizeof(GLfloat), vertices, 0);
    err = glGetError(); assert(!err);
    glCreateBuffers(1, &ibo);
    glNamedBufferStorage(ibo, indices_count * sizeof(GLuint), indices, 0);
    err = glGetError(); assert(!err);

    glCreateVertexArrays(1, &vao);
    err = glGetError(); assert(!err);

    glVertexArrayVertexBuffer(vao, 0, vbo, 0, 6 * sizeof(GLfloat)); //6 since we are using 1 array that is interleaved
    glVertexArrayElementBuffer(vao, ibo);
    err = glGetError(); assert(!err);

    glEnableVertexArrayAttrib(vao, 0);      //0 corresponds to the shader's layout(location=0)
    glEnableVertexArrayAttrib(vao, 1);
    //glEnableVertexArrayAttrib(vao, 2);
    err = glGetError(); assert(!err);

    glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, GL_FALSE, 0);
    glVertexArrayAttribFormat(vao, 1, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat) * 3);
    //glVertexArrayAttribFormat(vao, 2, 2, GL_FLOAT, GL_FALSE, offsetof(vertex_t, tex));
    err = glGetError(); assert(!err);

    glVertexArrayAttribBinding(vao, 0, 0);
    glVertexArrayAttribBinding(vao, 1, 0);
    //glVertexArrayAttribBinding(vao, 2, 0);
    err = glGetError(); assert(!err);

    glUseProgram(graphics_program);
    // Get a handle to fragment shader's iGlobalTime uniform.

    //transformation
    GLuint transformation_handle = glGetUniformLocation(graphics_program, "transformation");

    //tess ctrl
    GLuint tess_level_inner_handle = glGetUniformLocation(graphics_program, "tess_level_inner");
    GLuint tess_level_outer_handle = glGetUniformLocation(graphics_program, "tess_level_outer");

    //fragment
    GLuint global_time_handle = glGetUniformLocation(graphics_program, "iGlobalTime");
    GLuint resolution_handle = glGetUniformLocation(graphics_program, "iResolution");

    GLfloat time = 0.0f;

    //GLFW state tracking
    vec3 position = { 0.0f, 0.0f, 10.0f };
    vec3 up = { 0.0f, 1.0f, 0.0f };

    //depth
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    GLuint tess_level_inner = 4;
    GLuint tess_level_outer = 4;

    glUniform1f(tess_level_inner_handle, tess_level_inner);
    glUniform1f(tess_level_outer_handle, tess_level_outer);

    // Loop until the user closes the window 
    while (!glfwWindowShouldClose(window))
    {
        int width = 0, height = 0;
        glfwGetWindowSize(window, &width, &height);
        glUniform2f(resolution_handle, width, height);
        glUniform1f(global_time_handle, time);


        vec3 forward = { 0.0f, 0.0f, 1.0f };
        vec3 rotation = calculate_rotation(window);
        mat4 rotation_matrix = rotate(rotation);
        forward = multiply_mat4_vec3(&rotation_matrix, &forward);

        calculate_translation(window, &rotation, &position);

        //0.785398 radians == 45 degrees.
        //1.5708 radians == 90 degrees.
        mat4 projection = perspective(1.5708, (float)width / (float)height, 0.5f, 10000.0f);
        vec3 position_plus_forward = { position.x + forward.x, position.y + forward.y, position.z + forward.z };
        vec3 position_minus_forward = { position.x - forward.x, position.y - forward.y, position.z - forward.z };
        mat4 view = look_at(position, position_plus_forward, up);
        mat4 model = identity();
        mat4 model_view = multiply_mat4(&model, &view);
        mat4 model_view_projection = multiply_mat4(&model_view, &projection);
        glUniformMatrix4fv(transformation_handle, 1, GL_FALSE, &model_view_projection);
        
        glClearColor(0.01, 0.01, 0.01, 1.0);
        glClearDepth(1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Draw the image to the screen
		glUseProgram(graphics_program);
        glBindVertexArray(vao);
        glPatchParameteri(GL_PATCH_VERTICES, 3);       // tell OpenGL that every patch has 16 verts

        glDrawElementsInstanced(GL_PATCHES, indices_count, GL_UNSIGNED_INT, (void*)0, 10000);

		glfwSwapBuffers(window);
		glfwPollEvents();
        time += 0.001;
	}

    glDeleteVertexArrays(1, &vao);
    glfwTerminate();
    return;
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    GLint polygon_mode[2];
    glGetIntegerv(GL_POLYGON_MODE, polygon_mode);

    if ((key == GLFW_KEY_P) && (action == GLFW_PRESS))
    {
        if (polygon_mode[0] == GL_FILL)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        }
        else if (polygon_mode[0] == GL_LINE)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        else
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
    }

    if (((key == GLFW_KEY_UP) || (key == GLFW_KEY_DOWN) || (key == GLFW_KEY_LEFT) || (key == GLFW_KEY_RIGHT)) && (action == GLFW_PRESS))
    {
        GLuint tess_level_inner_handle = glGetUniformLocation(graphics_program, "tess_level_inner");
        GLuint tess_level_outer_handle = glGetUniformLocation(graphics_program, "tess_level_outer");

        GLfloat tess_level_inner;
        GLfloat tess_level_outer;
        glGetUniformfv(graphics_program, tess_level_inner_handle, &tess_level_inner);
        glGetUniformfv(graphics_program, tess_level_outer_handle, &tess_level_outer);

        if (key == GLFW_KEY_UP)
        {
            tess_level_inner += 1;
        }
        if (key == GLFW_KEY_DOWN)
        {
            tess_level_inner -= 1;
        }
        if (key == GLFW_KEY_RIGHT)
        {
            tess_level_outer += 1;
        }
        if (key == GLFW_KEY_LEFT)
        {
            tess_level_outer -= 1;
        }

        glUniform1f(tess_level_inner_handle, tess_level_inner);
        glUniform1f(tess_level_outer_handle, tess_level_outer);
    }

    if ((key == GLFW_KEY_ESCAPE) && (action == GLFW_PRESS))
    {
        glfwDestroyWindow(window);
    }
}

void window_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}
