#include <assert.h>
#include "program.h"

GLuint locations[64] = { 0 };

// returns an error code 
GLuint load_and_compile_shader(GLuint* shader, GLint type, char* filename)
{
    long lSize;
    char * buffer;
    size_t result;

    FILE * pFile = fopen(filename, "rb");

    if (pFile == NULL) { fputs("File error", stderr); exit(1); }

    // obtain file size:
    fseek(pFile, 0, SEEK_END);
    lSize = ftell(pFile);
    rewind(pFile);

    // allocate memory to contain the whole file:
    buffer = (char*)malloc(sizeof(char)*lSize + 1);
    buffer[lSize] = '\0';
    if (buffer == NULL) { fputs("Memory error", stderr); exit(2); }

    // copy the file into the buffer:
    result = fread(buffer, 1, lSize, pFile);
    if (result != lSize) { fputs("Reading error", stderr); exit(3); }

    /* the whole file is now loaded in the memory buffer. */

    // terminate
    fclose(pFile);
    buffer[lSize] = '\0';

    GLint isCompiled = 0;
    *shader = glCreateShader(type);
    glShaderSource(*shader, 1, &buffer, NULL);
    glCompileShader(*shader);
    glGetShaderiv(*shader, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(*shader, GL_INFO_LOG_LENGTH, &maxLength);

        //The maxLength includes the NULL character
        GLchar* infoLog = (GLchar*)malloc(maxLength * sizeof(GLchar));
        glGetShaderInfoLog(*shader, maxLength, &maxLength, &infoLog[0]);

        //We don't need the shader anymore.
        glDeleteShader(*shader);

        //Use the infoLog as you see fit.
        printf("%s", infoLog);

        assert(infoLog && 0);
        //In this simple program, we'll just leave
        return 1;
    }
    return 0;
}

GLuint link_program(GLuint program, GLuint vertex_shader, GLuint tess_ctrl_shader, GLuint tess_eval_shader, GLuint geometry_shader, GLuint fragment_shader, GLuint compute_shader)
{
    // Create program, attach shaders to it, and link it 
    if (vertex_shader) glAttachShader(program, vertex_shader);
    if (tess_ctrl_shader) glAttachShader(program, tess_ctrl_shader);
    if (tess_eval_shader) glAttachShader(program, tess_eval_shader);
    if (geometry_shader) glAttachShader(program, geometry_shader);
    if (fragment_shader) glAttachShader(program, fragment_shader);
    if (compute_shader) glAttachShader(program, compute_shader);
    glLinkProgram(program);
    GLint isLinked = 0;
    glGetProgramiv(program, GL_LINK_STATUS, &isLinked);
    if (isLinked == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

        //The maxLength includes the NULL character
        GLchar* infoLog = (GLchar*)malloc(maxLength * sizeof(GLchar));
        glGetProgramInfoLog(program, maxLength, &maxLength, &infoLog[0]);

        //The program is useless now. So delete it.
        glDeleteProgram(program);

        printf("%s", infoLog);

        assert(infoLog && 0);
        //Exit with failure.
        return 1;
    }
    return 0;
}

void build_graphics_program(GLuint* program)
{
    if (*program != 0)
    {
        glUseProgram(0);
        glDeleteProgram(*program);
        *program = 0;
    }
    GLuint vertex_shader = 0, tess_ctrl_shader = 0, tess_eval_shader = 0, geometry_shader = 0, fragment_shader = 0;
    load_and_compile_shader(&vertex_shader, GL_VERTEX_SHADER, "../../shaders/vertex.glsl");
    load_and_compile_shader(&tess_ctrl_shader, GL_TESS_CONTROL_SHADER, "../../shaders/tess_ctrl.glsl");
    load_and_compile_shader(&tess_eval_shader, GL_TESS_EVALUATION_SHADER, "../../shaders/tess_eval.glsl");
    load_and_compile_shader(&geometry_shader, GL_GEOMETRY_SHADER, "../../shaders/geometry.glsl");
    load_and_compile_shader(&fragment_shader, GL_FRAGMENT_SHADER, "../../shaders/fragment.glsl");
    *program = glCreateProgram();
    link_program(*program, vertex_shader, tess_ctrl_shader, tess_eval_shader, geometry_shader, fragment_shader, 0);
    if (vertex_shader) glDeleteShader(vertex_shader);
    if (tess_ctrl_shader) glDeleteShader(tess_ctrl_shader);
    if (tess_eval_shader) glDeleteShader(tess_eval_shader);
    if (geometry_shader) glDeleteShader(geometry_shader);
    if (fragment_shader) glDeleteShader(fragment_shader);

    glUseProgram(*program);
}
