#version 450 core

uniform float iGlobalTime;
uniform vec2 iResolution;

in vec3 g_color;
out vec4 color;

void main()
{
    color = vec4(g_color ,1.0);
}