#version 450 core

layout(location=0) in vec3 position;
layout(location=1) in vec3 color;

out vec3 vs_position;
out vec3 vs_color;
out int vs_instance_id;

void main()
{
	vs_instance_id = gl_InstanceID;
	vs_position = position;
	vs_color = vec3((gl_InstanceID % 10000) / 10000.0, (gl_InstanceID % 5000) / 5000.0, (gl_InstanceID % 2500) / 2500.0);//color;
}