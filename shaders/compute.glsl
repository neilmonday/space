#version 450 core

#define PI 3.1415926535897932384626433832795
#define E 2.7182818284590452353602874713526
#define MAX_T 99999999.0
#define SPHERE 1
#define TRIANGLE 2
#define TEMPERATURE 5500 /* Hidden temperature argument to BB_SPECTRUM.*/
#define MAX_SAMPLE_COUNT 4

struct Shape
{
    mat4 transform;
    vec4 v0;
    vec4 v1;
    vec4 v2;
    float power;
    uint type;
    uint color_index;
    uint padding;
};

struct Ray
{
    vec3 origin;
    vec3 direction;
    float t;
};

//A beam is just a ray with additional parameters I want to carry along with the light.
struct Beam
{
    //the shape that the beam originated from. This is
    //used to skip a shape during raycast
    uint shape;
    float falloff;
    float power;            //amplify the emissiveSpectrum values.
    float distance;         //total distance of the beam from camera to light
    Ray ray;
    bool terminated;
};

Beam my_beam;
Ray temp_ray;
float temp_falloff;
uint modified_seed;

//Does not change from the start of the application
layout (local_size_x = 16, local_size_y = 16) in;
layout (location=0, rgba32f) uniform image2D image;
layout (location=1) uniform uint shapes_count;

//Does not change from the start of the frame
layout (location=2) uniform mat4 transform;

//Does not change from the start of the macro group
//macro_group_size is exactly the same as gl_NumWorkGroups
layout (location=3) uniform uvec3 macro_group_id;
layout (location=4) uniform uvec3 num_macro_groups;

//Changes with each iteration of wavelength
layout (location=5) uniform float seed;
layout (location=6) uniform uint wavelength;
layout (location=7) uniform vec3 color_match;
layout (location=8) uniform float reflective_strength[4];

layout (binding=0, std140) uniform ShapeData
{
    Shape shapes[32];
};

/*http://www.fourmilab.ch/documents/specrend/*/
/*struct ColorSystem {
    vec4 x;
    vec4 y;
};*/
                                     //x    r       g       b       white        y    r       g       b       white
/*ColorSystem NTSCsystem   = ColorSystem(vec4(0.67,   0.21,   0.14,   0.3101),     vec4(0.33,   0.71,   0.08,   0.3162));
ColorSystem EBUsystem    = ColorSystem(vec4(0.64,   0.29,   0.15,   0.3127),     vec4(0.33,   0.60,   0.06,   0.3291));
ColorSystem HDTVsystem   = ColorSystem(vec4(0.67,   0.21,   0.15,   0.3127),     vec4(0.33,   0.71,   0.06,   0.3291));
ColorSystem CIEsystem    = ColorSystem(vec4(0.7355, 0.2658, 0.1669, 0.33333333), vec4(0.2645, 0.7243, 0.0085, 0.33333333));
ColorSystem Rec709system = ColorSystem(vec4(0.64,   0.30,   0.15,   0.3127),     vec4(0.33,   0.60,   0.06,   0.3291));*/

float bb_spectrum(float wavelength)
{
    float wlm = wavelength * 1e-9;   /* Wavelength in meters */

    return (3.74183e-16 * pow(wlm, -5.0)) /
           (exp(1.4388e-2 / (wlm * TEMPERATURE)) - 1.0);
}

vec3 spectrum_to_rgb(float emissiveSpectrum)
{
    int i;
    float Me;

    Me = bb_spectrum(wavelength);
    float xyz_input_x = Me * color_match.x * emissiveSpectrum;
    float xyz_input_y = Me * color_match.y * emissiveSpectrum;
    float xyz_input_z = Me * color_match.z * emissiveSpectrum;

	vec3 result;
    vec4 x, y, z;
    vec4 r, g, b;

    //NTSCsystem is hard coded into these values
    x.r = 0.67;
    y.r = 0.33;
    z.r = 1 - (x.r + y.r);
    x.g = 0.21;
    y.g = 0.71;
    z.g = 1 - (x.g + y.g);
    x.b = 0.14;
    y.b = 0.08;
    z.b = 1 - (x.b + y.b);
    x.w = 0.3101;
    y.w = 0.3162;
    z.w = 1 - (x.w + y.w);

    r.x = (y.g * z.b) - (y.b * z.g);
    r.y = (x.b * z.g) - (x.g * z.b);
    r.z = (x.g * y.b) - (x.b * y.g);
    g.x = (y.b * z.r) - (y.r * z.b);
    g.y = (x.r * z.b) - (x.b * z.r);
    g.z = (x.b * y.r) - (x.r * y.b);
    b.x = (y.r * z.g) - (y.g * z.r);
    b.y = (x.g * z.r) - (x.r * z.g);
    b.z = (x.r * y.g) - (x.g * y.r);

    r.w = ((r.x * x.w) + (r.y * y.w) + (r.z * z.w)) / y.w;
    g.w = ((g.x * x.w) + (g.y * y.w) + (g.z * z.w)) / y.w;
    b.w = ((b.x * x.w) + (b.y * y.w) + (b.z * z.w)) / y.w;

    r.xyz = r.xyz/r.w;
    g.xyz = g.xyz/g.w;
    b.xyz = b.xyz/b.w;

    result.r = (r.x * xyz_input_x) + (r.y * xyz_input_y) + (r.z * xyz_input_z);
    result.g = (g.x * xyz_input_x) + (g.y * xyz_input_y) + (g.z * xyz_input_z);
    result.b = (b.x * xyz_input_x) + (b.y * xyz_input_y) + (b.z * xyz_input_z);

	return result;
}

uint hash( uint x ) {
    x += ( x << 10u );
    x ^= ( x >>  6u );
    x += ( x <<  3u );
    x ^= ( x >> 11u );
    x += ( x << 15u );
    return x;
}

// Compound versions of the hashing algorithm I whipped together.
uint hash( uvec2 v ) { return hash( v.x ^ hash(v.y)                         ); }
uint hash( uvec3 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z)             ); }
uint hash( uvec4 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z) ^ hash(v.w) ); }

// Construct a float with half-open range [0:1] using low 23 bits.
// All zeroes yields 0.0, all ones yields the next smallest representable value below 1.0.
float floatConstruct( uint m ) {
    const uint ieeeMantissa = 0x007FFFFFu; // binary32 mantissa bitmask
    const uint ieeeOne      = 0x3F800000u; // 1.0 in IEEE binary32

    m &= ieeeMantissa;                     // Keep only mantissa bits (fractional part)
    m |= ieeeOne;                          // Add fractional part to 1.0

    float  f = uintBitsToFloat( m );       // Range [1:2]
    //return f - 1.0;                        // Range [0:1]
    return ((f - 1.0) * 2.0) - 1.0;        // Range [-1:1]
}

// Pseudo-random value in half-open range [0:1].
float random( float x ) { return floatConstruct(hash(floatBitsToUint(x))); }
float random( vec2  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float random( vec3  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float random( vec4  v ) { return floatConstruct(hash(floatBitsToUint(v))); }

void reflectBeam(vec3 normal, float t, uint i)
{

    /*if(i > 22)//this is a hack to make the tall block mirror finish
    {
        //MAKE IT SPECULAR
        beam.ray.direction = normalize(reflect(normalize(d), normalize(normal)));
    }
    else*/
    {
        //MAKE IT DIFFUSE
        vec3 random = vec3(random(vec3(modified_seed, i, seed)),
                                    random(vec3(seed, modified_seed, i)) ,
                                    random(vec3(i, seed, modified_seed)));
        if(dot(random, normal) < 0)
        {
            random = -random;
        }
        temp_ray.direction = random;
    }

    //how much power is lost due to reflection.
    //1.  The dot product calculates how much power is lost due  
    //    to the incident angle (the rays are traced from the 
    //    camera so the incident angle is calculated above and 
    //    exitance angle is passed in)
    //2.  falloff keeps track of this ray's power
    //    lost throughout its entire journey
    //3.  reflective_strength gets the color strength of each
    //    shape. A green surface reflects green better than
    //    other colors
    temp_falloff = dot( temp_ray.direction, normal) * my_beam.falloff * reflective_strength[shapes[i].color_index];

    //old way.((dot(normal, beam.ray.direction) * attenuation)/(4.0 * PI * t * t));
    temp_ray.origin = my_beam.ray.origin + t * my_beam.ray.direction;
    
    //beam.ray.origin += normalize(beam.ray.direction);
    temp_ray.t = t; 

    // this makes sure that the new ray cast doesn't hit this same object
    return;
}

void intersectSphere(uint i)
{
    const float r = shapes[i].v0.x;

    //sphere equation (x-f.x)^2 + (y-f.y)^2 + (z-f.z)^2 - r*r = 0
    //parametric rays plugged into sphere equation
    // (o.x + td.x)^2 + (o.y + td.y)^2 + (o.z + td.z)^2 - r*r = 0
    // (o.x + td.x) * (o.x + td.x) + (o.y + td.y) * (o.y + td.y) + (o.z + td.z) * (o.z + td.z) - 9 = 0
    // (o.x)^2 + 2(o.x*td.x) + (td.x)^2 + (o.y)^2 + 2(o.y*td.y) + (td.y)^2 + (o.z)^2 + 2(o.z*td.z) + (td.z)^2 - 9 = 0
    // o.x^2 + o.y^2 + o.z^2 + 2(o.x*td.x) + 2(o.y*td.y) + 2(o.z*td.z) + (td.x)^2 + (td.y)^2 + (td.z)^2 - 9 = 0

    //quadratic equation ax^2 + bx + c = 0
    // gather all of the constants for c
    const float c =
        (my_beam.ray.origin.x-shapes[i].transform[3].x) * (my_beam.ray.origin.x-shapes[i].transform[3].x) +
        (my_beam.ray.origin.y-shapes[i].transform[3].y) * (my_beam.ray.origin.y-shapes[i].transform[3].y) +
        (my_beam.ray.origin.z-shapes[i].transform[3].z) * (my_beam.ray.origin.z-shapes[i].transform[3].z) -
        r * r;

    const float b =
        2 * (my_beam.ray.origin.x-shapes[i].transform[3].x) * my_beam.ray.direction.x +
        2 * (my_beam.ray.origin.y-shapes[i].transform[3].y) * my_beam.ray.direction.y +
        2 * (my_beam.ray.origin.z-shapes[i].transform[3].z) * my_beam.ray.direction.z;

    // t * t * d.x * d.x + t * t * d.y * d.y + t * t * d.z * d.z
    // t * t (d.x * d.x + d.y * d.y + d.z * d.z)
    const float a = my_beam.ray.direction.x * my_beam.ray.direction.x + 
        my_beam.ray.direction.y * my_beam.ray.direction.y + 
        my_beam.ray.direction.z * my_beam.ray.direction.z;

    //see if our ray intersects with the sphere
    //(i.e. does quadratic forumla have roots)
    float determinant = b*b - 4*a*c;

    if (determinant > 0)
    {
        // quadratic formula
        const float t1 = (-b + sqrt(determinant))/(2 * a);
        const float t2 = (-b - sqrt(determinant))/(2 * a);

        const float new_t = min(t1, t2);
        if ((0 < new_t) && (new_t < my_beam.ray.t))
        {
            vec3 new_normal = normalize((my_beam.ray.origin + new_t * my_beam.ray.direction) - shapes[i].transform[3].xyz);
            reflectBeam(new_normal, new_t, i);
        }
    }
    return;
}

void intersectTriangle(uint i)
{
    // define the edges so that they are tip-to-tail
    vec3 edge0 = vec3(shapes[i].v1 - shapes[i].v0);
    vec3 edge1 = vec3(shapes[i].v2 - shapes[i].v1);
    vec3 edge2 = vec3(shapes[i].v0 - shapes[i].v2);

    // this is the triangle's normal
    vec3 new_normal = normalize(cross(edge0, -edge2));

    //Ray: P = o + t * d;
    //Plane: Ax + By + Cz + D = 0
    //A(o.x + t*d.x) + B(o.y + t*d.y) + C(o.z + t*d.z) + D = 0
    //A*t*d.x + B*t*d.y + C*t*d.z + A*o.x + B*o.y + C*o.z + D = 0
    //t * (A*d.x + B*d.y + C*d.z) + A*o.x + B*o.y + C*o.z + D = 0

    //D = -Ax - By - Cz;
    float a = new_normal.x;
    float b = new_normal.y;
    float c = new_normal.z;
    float D = -a*shapes[i].v0.x - b*shapes[i].v0.y - c*shapes[i].v0.z;
    float new_t = -(a*my_beam.ray.origin.x + b*my_beam.ray.origin.y + c*my_beam.ray.origin.z + D) / 
        (a*my_beam.ray.direction.x + b*my_beam.ray.direction.y + c*my_beam.ray.direction.z);

    // P is where the ray intersects with the plane
    vec3 P = vec3(my_beam.ray.origin) + new_t * vec3(my_beam.ray.direction);
    vec3 C0 = P - vec3(shapes[i].v0);
    vec3 C1 = P - vec3(shapes[i].v1);
    vec3 C2 = P - vec3(shapes[i].v2);

    // if point P is outside of the triangle, then go onto the next triangle
    if ((dot(new_normal, cross(edge0, C0)) > 0 &&
        dot(new_normal, cross(edge1, C1)) > 0 &&
        dot(new_normal, cross(edge2, C2)) > 0) &&
        (0 < new_t) &&
        (new_t < my_beam.ray.t))
    {
        reflectBeam(new_normal, new_t, i);
    }
    return;
}

void intersect()
{
    //Ray components
    vec3 origin;
    vec3 direction;
    float t = MAX_T;

    //Which shape is closest
    int shape_index;
    float falloff;

    for(int i=0; i<shapes_count; i++)
    {
        //Ray temp_ray;
        temp_ray.t = MAX_T;
        if(shapes[i].type == SPHERE)
        {
            intersectSphere(i);
        }
        else if(shapes[i].type == TRIANGLE)
        {
            intersectTriangle(i);
        }

        //we don't want to intersect with the same shape twice
        if(my_beam.shape == i)
        {
            //This causes divergent control flow?? I can just
            //set temp_ray.t to max as another way to ignore
            //this shape, and the control flow for each thread
            //should be identical making it faster???
            //continue;
            temp_ray.t = MAX_T;
        }

        //The shortest ray is the closest object
        if(temp_ray.t < t)
        {
            origin = temp_ray.origin;
            direction = temp_ray.direction;
            t = temp_ray.t;
            shape_index = i;
            falloff = temp_falloff;
        }
    }

    //my_beam = temp;
    my_beam.ray.origin = origin;
    my_beam.ray.direction = direction;
    my_beam.ray.t = t;
    my_beam.distance += t;
    my_beam.shape = shape_index;
    my_beam.falloff = falloff;
    my_beam.power = shapes[shape_index].power/(4 * PI * my_beam.distance * my_beam.distance);
    my_beam.terminated = (shapes[shape_index].power > 0.0) && (my_beam.ray.t > 0) && (my_beam.ray.t < MAX_T);

    return;
}

void main()
{
    //size of screen (in screen coordinates)
    const uvec2 size = uvec2(
        num_macro_groups.x * gl_NumWorkGroups.x * gl_WorkGroupSize.x,
        num_macro_groups.y * gl_NumWorkGroups.y * gl_WorkGroupSize.y);

    //position in screen coordinates
    const ivec2 position = ivec2(
        gl_LocalInvocationID.x + (gl_WorkGroupID.x * gl_WorkGroupSize.x) + (macro_group_id.x * gl_NumWorkGroups.x * gl_WorkGroupSize.x),
        gl_LocalInvocationID.y + (gl_WorkGroupID.y * gl_WorkGroupSize.y) + (macro_group_id.y * gl_NumWorkGroups.y * gl_WorkGroupSize.y));


	float final_spectrum = 0.0;

    //this is for getting multiple samples for a pixel
    //this tends to get some weird horizontal stretched colors.
    for(int sample_iteration = 0; sample_iteration < MAX_SAMPLE_COUNT; sample_iteration++)
    {
        //generate a unique seed
        modified_seed = position.x;
        modified_seed += position.y * size.x;
        modified_seed += sample_iteration * size.y * size.x;
        vec2 random_offset = vec2(random(modified_seed * seed), random((modified_seed+1 * seed)));
        
        //Setting up the ray from the camera out into the world.
        my_beam.ray.direction = vec3(transform * normalize(
            vec4(position + random_offset - ivec2(size.x/2,size.y/2),
            -2000.0,//this dictates how wide angle the lens is
            0.0)
        ));
        my_beam.ray.origin = vec3(transform * vec4(0.0, 0.0, 0.0, 1.0));
        my_beam.ray.t = MAX_T;

        my_beam.distance = 0;
        my_beam.shape = shapes_count;
        my_beam.terminated = false;
        my_beam.power = 0.0;
        my_beam.falloff = 1.0;

        uint iteration = 0;
        while( (my_beam.terminated == false) && (iteration < 4))
        {
            modified_seed += iteration * size.y * size.x;

            intersect();
            if((my_beam.ray.t >= MAX_T) || (my_beam.ray.t <= 1.0))
            {
                iteration = 4;
            }

            my_beam.ray.t = MAX_T; //reset t for the next ray
            iteration++;
        }

        final_spectrum += my_beam.falloff * my_beam.power;

    }
	vec3 rgb = spectrum_to_rgb(final_spectrum);
	//vec3 rgb = xyz_to_rgb(/*HDTVsystem,*/ xyz);

    vec4 pixel = imageLoad(image, position);
    pixel.rgb *= pixel.a;
    pixel.a += 1.0/76.0;
    pixel.rgb += rgb;
    pixel.rgb /= pixel.a;
    imageStore(image, position, pixel);

    /*vec4 pixel = vec4(rgb, 1.0);
    imageStore(image, position, pixel);*/
}
